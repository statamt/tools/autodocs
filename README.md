# autodocs

Annotated bash script automatically converted to markdown.

- Comments are disregarded, unless the start with `# --..--`.
- ignoring entire blocks can be done using `# --//-- START` and ended with `# --//-- END`
- `sleep`, `echo`, `set` commands are excluded

Some extras, that are specific to our script:

- `$kubectl`, `$minikube` are being replaced with `kibectl` and `minikube`.

## usage

```bash
docker run --user=1000 --rm -it -v $(pwd):/data registry.gitlab.com/statamt/tools/autodocs/autodoc -i /data/examples/test.sh -o -
```

See `compose.yml` for an example with docker compose.

## Development

Prerequisites:

- docker
- docker compose

Typical steps:

1. adapt examples/test.sh with the feature you'd like.
2. adapt autodoc.py
3. "Test the script" (manual tests only, sorry) `docker compose up`
4. Rinse, repeat.
