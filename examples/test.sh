#!/bin/bash

# --..-- # First Line is a title
# --..--
# --..-- Empty lines are kept
# this line is ignore
# --..-- this one is kept
# --..--no space needed
# --..--

# should not show up
set -e

# comments do not show up

# echo 

echo 'hello world'

# not in docs:
# --//-- START comment

# --..--
# --..-- this is ignored!
# --..--

echo 'theses lines do not have any content in the output'
echo '# --//-- END does not break anything'
echo '# --..-- this is ignored as well'

# --..--
# --..-- this is still ignored!
# --..--

echo '# --..-- this is ignored as well'

# --//-- END comment

# --..-- First line after comments
