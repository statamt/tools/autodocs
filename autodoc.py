#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import os
from pathlib import Path

SCRIPT_PATH = Path(os.path.dirname(os.path.abspath(__file__))).absolute()
BASE_PATH = SCRIPT_PATH.parent

IGNORED_PARTS = ['echo', 'set', 'sleep', '#']

REPLACE_STRINGS = {
    '$kubectl': 'kubectl',
    '$minikube': 'minikube',
}

AUTO_DOC_SEPARATOR = '# --..--'
AUTO_DOC_IGNORE_START = '# --//-- START'
AUTO_DOC_IGNORE_END = '# --//-- END'


def _line_ignored(line: str, ignored: bool):
    # ignore all comments
    # and outputs that are for running the script only
    if ignored:
        return True
    line_content = line.strip()
    if any([line_content.startswith(kw) for kw in IGNORED_PARTS]):
        return True
    # ignore empty lines
    if line_content == '':
        return True
    return False


def create_setupdoc(bash_file_path: Path, output_file_path: Path | None = None, quiet: bool = False):
    content = ''
    ignored = False
    with bash_file_path.open('r') as f:
        code_started = False
        for line in f:
            cleaned_line = line.lstrip()
            for key, value in REPLACE_STRINGS.items():
                cleaned_line = cleaned_line.replace(key, value)

            if line.startswith(AUTO_DOC_SEPARATOR) and not ignored:
                if code_started:
                    content += '```\n\n'
                    code_started = False
                # text content
                cleaned_line = cleaned_line.split(AUTO_DOC_SEPARATOR)[-1]
                if cleaned_line == '':
                    content += '\n'
                else:
                    content += cleaned_line
            elif cleaned_line.startswith(AUTO_DOC_IGNORE_START):
                ignored = True
            elif cleaned_line.startswith(AUTO_DOC_IGNORE_END):
                ignored = False
            elif _line_ignored(cleaned_line, ignored):
                continue
            else:
                if not code_started:    
                    content += '```bash\n'
                    code_started = True
                content += cleaned_line
    content += '\n'

    if not quiet:
        print(content)

    if output_file_path is not None:
        with output_file_path.open('w') as f:
            f.write(content)
        print(f'\n\n processed input \n{bash_file_path}\n and saved to \n{output_file_path}')


if __name__ == '__main__':
    ignore_string = ', '.join([f'`{part}`' for part in IGNORED_PARTS])
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        prog='autodoc.py',
        description=f'''
    Converts a bash file with specific comments to markdown.
    
    In the bash file:
    - add `# --..--` to add text blocks.
    - add `# --//-- START` and `# --//-- END` to ignore a block of code.
    - all  {ignore_string} and comments without the above syntax are ignored.

    To output to the console:
$ ./scripts/autodoc.py -i setup.sh -o -
    
    Examples:
$ ./scripts/autodoc.py --help
$ ./scripts/autodoc.py -i setup.sh -o docs/setup.md
''',
        # epilog='Example: ./scripts/autodoc.py -i setup.sh -o docs/setup.md',
    )
    parser.add_argument('-i', '--input', help='Bash file path', type=str)
    parser.add_argument('-o', '--out', help='Output file path', type=str)
    parser.add_argument('-q', '--quiet', help='do not output result', type=bool, default=False)
    args = parser.parse_args()
    out = Path(args.out) if args.out != '-' else None
    create_setupdoc(Path(args.input), out, args.quiet)
