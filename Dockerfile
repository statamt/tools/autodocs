FROM python:3.12

ADD autodoc.py /usr/local/bin/autodoc.py

RUN useradd -ms /bin/bash autodoc

ENTRYPOINT [ "/usr/local/bin/autodoc.py" ]

CMD [ "--help" ]
